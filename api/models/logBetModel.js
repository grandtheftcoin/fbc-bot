var db = require('./../../dbconnection');
var LogBets = {
    data : {},
    save : function(callback){
        var logBet = this.data;
        console.log('model save');
        console.log('logBet', JSON.stringify(logBet));
        return db.query("insert into log_bet (batch, bet_amount,bet_choice, roll_number,is_win,btc_before,btc_after,rp_before,rp_after,bonus_inc_percent_before, bonus_inc_percent_after,min_reward,bonus_balance_before,bonus_balance_after,bonus_wager_remaining_before, bonus_wager_remaining_after, bet_strategy, time) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, NOW())",
        [
            logBet.batch,
            logBet.bet_amount, 
            logBet.bet_choice, 
            logBet.roll_number, 
            logBet.is_win, 
            logBet.btc_before, 
            logBet.btc_after, 
            logBet.rp_before, 
            logBet.rp_after,
            logBet.bonus_inc_percent_before,
            logBet.bonus_inc_percent_after,
            logBet.min_reward,
            logBet.bonus_balance_before,
            logBet.bonus_balance_after,
            logBet.bonus_wager_remaining_before,
            logBet.bonus_wager_remaining_after,
            logBet.bet_strategy
        ],
        callback);
    },
    find : function(callback){
        console.log('model find');
        return db.query("select * from log_bet", callback);
    }
};

module.exports = LogBets;