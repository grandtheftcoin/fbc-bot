var db = require('./../../dbconnection');
var LogRolls = {
    data : {},
    save : function(callback){
        var logRoll = this.data;
        console.log('model save');
        console.log('logRoll', JSON.stringify(logRoll));
        return db.query("insert into log_roll (roll_number, won_btc, won_rp, rp_before, rp_after, btc_before, btc_after, bonus_inc_percent_before, bonus_inc_percent_after, time) values(?,?,?,?,?,?,?,?,?, NOW())",
        [
            logRoll.roll_number,
            logRoll.won_btc, 
            logRoll.won_rp, 
            logRoll.rp_before, 
            logRoll.rp_after, 
            logRoll.btc_before, 
            logRoll.btc_after, 
            logRoll.bonus_inc_percent_before, 
            logRoll.bonus_inc_percent_after
            
        ],
        callback);
    },
    find : function(callback){
        console.log('model find');
        return db.query("select * from log_roll", callback);
    }
};

module.exports = LogRolls;