module.exports = function(app){
    var logBetController = require('./../controllers/logBetController');
    var logRollController = require('./../controllers/logRollController');
    app.route('/log_bet')
            .get(logBetController.get)
            .post(logBetController.create);
    
    app.route('/log_roll')
            .get(logRollController.get)
            .post(logRollController.create);
}