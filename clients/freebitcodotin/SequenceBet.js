var sequenceBet = (function(){
	/*
	 * mode
	 * high : 0
	 * low : 1
	 * alternateHigh : 2
	 * alternateLow : 3
	 */
	var MODE_HIGH = 0;
	var MODE_LOW = 1;
	var MODE_ALTERNATE_HIGH = 2;
	var MODE_ALTERNATE_LOW = 3;
        var MODE_ALTERNATE_NOW = 4;
	var showLog = false;
        
        var TRANSLATE_MODE = ['MODE_HIGH', 'MODE_LOW', 'MODE_ALTERNATE_HIGH', 'MODE_ALTERNATE_LOW', 'MODE_ALTERNATE_NOW'];
	
	var currentMode = null;
	var counter = 1;
	return {
		setModeHigh : function(){
			this.setMode(MODE_HIGH);
		},
		setModeLow : function(){
			this.setMode(MODE_LOW);
		},
		setModeAlternateHigh : function(){
			this.setMode(MODE_ALTERNATE_HIGH);
		},
		setModeAlternateLow : function(){
			this.setMode(MODE_ALTERNATE_LOW);
		},
                setModeAlternateNow : function(){
			this.setMode(MODE_ALTERNATE_NOW);
		},
		setMode : function(mode){
			if(showLog)console.log('set mode ', mode);
			if(currentMode == mode)return;
			currentMode = mode;
			counter = 1;
		},
		getMode : function(){
			return currentMode;
		},
                getModeText : function(){
                    return TRANSLATE_MODE[currentMode];
                },
                inverseBet : function(){
                    //mode alternate now
                    if(currentMode == MODE_ALTERNATE_NOW){
                        if(trend.getLastRoll() < 5000){
                            return manualBet.betHigh();
                        }
                        else{
                            return manualBet.betLow();
                        }
                    }
                    else{
                        console.log('inverseBet error');
                    }
                },
		nextBet : function(isPrevWin){
                        
                    
                    
                        if(isPrevWin){
                            counter++;
                        }
                        else{
                            counter = 1;
                        }
			
			var isOdd = (counter % 2) == 1;
			
			if(currentMode == MODE_HIGH){
				if(showLog)console.log('bet high');
				return manualBet.betHigh();
			}
			else if(currentMode == MODE_LOW){
				if(showLog)console.log('bet low');
				return manualBet.betLow();
			}
			else if(currentMode == MODE_ALTERNATE_HIGH){
                                if(!isPrevWin){
                                    if(showLog)console.log('bet high');
                                    return manualBet.betHigh();
                                }
				
                                if(isOdd){
					if(showLog)console.log('bet high');
					return manualBet.betHigh();
				}
				else{
					if(showLog)console.log('bet low');
					return manualBet.betLow();
				}
                                
				
			}
			else if (currentMode == MODE_ALTERNATE_LOW){
                                if(!isPrevWin){
                                    if(showLog)console.log('bet low');
                                    return manualBet.betLow();
                                }
                                
				if(isOdd){
					if(showLog)console.log('bet low');
					return manualBet.betLow();
				}
				else{
					if(showLog)console.log('bet high');
					return manualBet.betHigh();
				}
			}
			else{
				if(showLog)console.log('undefined mode');
			}
			
		}
	}
})();