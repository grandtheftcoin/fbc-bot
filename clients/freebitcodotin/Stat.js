var stat = (function(){
	var counterWin = 0;
	var counterLose = 0;
	var counter = 0;
	var sumWager = 0;
	
	return{
		addWin : function(){
			counter++;
			counterWin++;
		},
		addLose : function(){
			counter++;
			counterLose++;
		},
		addWager : function(wager){
			sumWager+= wager;
		}, 
		show : function(){
			console.log('========================');
			console.log('sumWager : '+sumWager );
			console.log('counter : '+counter );
			console.log('counterWin : '+counterWin );
			console.log('counterLose : '+counterLose );
			console.log('winRate : '+ (counterWin/counter)*100 );
			console.log('========================');
		}
	}
})();