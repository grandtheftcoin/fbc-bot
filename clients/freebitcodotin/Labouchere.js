var Labourchere = (function(){
  var betList = null;
  var showLog = false;
  var counterSwitch = 0;
  var counterBet = 0;
  var tempTrend = 0;

  var accountBalanceBefore = 0;
  var accountBalance = 0;
  var accountBalanceAfter = 0;
  var self = this;

  var prevResult = true;
  var prevBTC = null;
  var prevRP = null;
  var betBatch = null;
  
  var timer = null;
  
  var stop = function(){
      clearInterval(timer);
  };

  var init = function(){
      counterBet=0;
      if(localStorage.getItem('betList') == null || localStorage.getItem('betList') == 'null'){
        betList = [];
        for(var i=0;i<10;i++){
            betList.push("0.00000010");
        }
      }
      else{
        betList = JSON.parse(localStorage.getItem('betList'));
      }

      if((localStorage.getItem('betBatch') == null) || (localStorage.getItem('betBatch') == 'null')){
          localStorage.setItem('betBatch', 1);
          betBatch = 1;
      }
      else{
          betBatch = localStorage.getItem('betBatch');
          betBatch++;
          localStorage.setItem('betBatch', betBatch);
      }
      
      sequenceBet.setModeAlternateNow();
  };

  var start = function(){
      
    var waiting = false;
    init();
 
    accountBalanceBefore = Freebitcoin.getBalance();
    accountBalanceBefore = parseFloat(accountBalanceBefore);
    accountBalance = accountBalanceBefore;
    
    var percentBefore = Freebitcoin.getBonusIncreasePercent();
    var bonusBalanceBefore = Freebitcoin.getBonusBalance();
    var bonusWagerRemainingBefore = Freebitcoin.getBonusWagerRemaining();
    
    
    
    timer = setInterval(function(){
      if(waiting)return;
      
//      if(counterSwitch >= 13){
//    	  counterSwitch = 0
//    	  tempTrend = trend.countLastTrend(21);
//    	  if(showLog)console.log('get trend', tempTrend);
//      }
//      else{
//    	  counterSwitch++;
//    	  if(showLog)console.log('counter switch', counterSwitch);
//      }
      
//      switch(tempTrend){
//      	case 0 : sequenceBet.setModeHigh(); break;
//      	case 1 : sequenceBet.setModeLow(); break;
//      	case 2 : sequenceBet.setModeAlternateHigh(); break;
//      	case 3 : sequenceBet.setModeAlternateLow(); break;
//      	default : sequenceBet.setModeLow();break;
//      }
      
      if(betList.length == 0){
        if(showLog)console.log('=== batch done ===');
        localStorage.removeItem('betList');
//        accountBalanceAfter = accountBalance;
//        if(showLog)console.log('accountBalanceAfter : '+ accountBalanceAfter.toFixed(8).toString());
        clearInterval(timer);
        waiting = false;
//        trend.countTrend();
        Bot.resetBet();
      }
      else{
        waiting = true;
        var betAmount = null;
        if(betList.length == 1){
            betAmount = betList[0];
        }
        else{
            betAmount = parseFloat(betList[0]) + parseFloat(betList[betList.length-1]);
            betAmount = betAmount.toFixed(8).toString();
        }
        
        //checking balance before bet.
        if(parseFloat(betAmount) > accountBalance){
            console.log('no money no cry');
            
                  
                  $.ajax({
                    method: "POST",
                    url: "http://127.0.0.1:3001/alert",
                    data: {
                        btc : Freebitcoin.getBalance(), 
                        counter : counterBet
                    }
                    });
                    
                
            return;
        }
        
        accountBalance -= parseFloat(betAmount);
        prevBTC = Freebitcoin.getBalance();
        prevRP = Freebitcoin.getRewardPoint();

        
        

        if(showLog)console.log('bet amount : '+betAmount);
        stat.addWager(parseFloat(betAmount));
        
        manualBet.openBetTab()
        .then(manualBet.setBetAmount(betAmount))
        //set
        .then(function(){
//        	return sequenceBet.nextBet(prevResult);
        	return sequenceBet.inverseBet();
        })
        
        .then(function(betRet){
            var isWin = betRet.result;
            prevResult = isWin;
          if(showLog)console.log('bet resutl :'+isWin);
          
          if(isWin == undefined){
//        	  console.log(isWin);
        	  console.log('system error');
        	  console.log(isWin);
        	  return;
          }
          
         
          
          var promise = new Promise(function(resolve, reject){
               if(isWin){
                    stat.addWin();
                    betList.shift();
                    betList.pop();
                    accountBalance += (parseFloat(betAmount)*2)
                  }
                  else{
                    stat.addLose();
                    betList.push(betAmount);
                  }
                  if(showLog)console.log('list : '+ betList);
                  if(showLog)console.log('list.length : '+ betList.length);
                  if(showLog)console.log('accountBalanceBefore : '+ accountBalanceBefore.toFixed(8).toString());
                  if(showLog)console.log('accountBalance : '+ accountBalance.toFixed(8).toString());
                  
                  localStorage.setItem('betList', JSON.stringify(betList));
                  
                  $.ajax({
                    method: "POST",
                    url: "http://127.0.0.1:3000/log_bet",
                    data: {
                        batch : betBatch,
                        bet_amount: parseFloat(betAmount), 
                        bet_choice: betRet.type, 
                        roll_number: Freebitcoin.getBetRollNumber(), 
                        is_win: isWin?1:0,
                        btc_before: prevBTC, 
                        btc_after: Freebitcoin.getBalance(), 
                        rp_before: prevRP, 
                        rp_after: Freebitcoin.getRewardPoint(),
                        bonus_inc_percent_before : percentBefore,
                        bonus_inc_percent_after : Freebitcoin.getBonusIncreasePercent(),
                        min_reward : Freebitcoin.getMinReward(),
                        bonus_balance_before : bonusBalanceBefore,
                        bonus_balance_after : Freebitcoin.getBonusBalance(),
                        bonus_wager_remaining_before : bonusWagerRemainingBefore,
                        bonus_wager_remaining_after : Freebitcoin.getBonusWagerRemaining(),
                        bet_strategy : sequenceBet.getModeText()
                    }
                    })
                    .done(function( msg ) {
                      waiting = false;
                      console.log("save done");
                      counterBet++;
                      resolve();
                    })
                    .error(function(request, status, error){
                        console.log("ajax error : "+error);
                        stop();
                    });
                  
          });
          return promise
        })
        .then(function(){
            console.log("stat");
            stat.show();
        });
        ;
      }
      }, 3000);
  }

  return{
    start : start,
    setLog : function(status){
      showLog = status;
    },
    stop : stop,
    cron : function(){
      var done = false;

      var timer = setInterval(function(){
        var countdownMinute = $('#time_remaining .countdown_amount').eq(0).html();
        if(showLog)console.log('countdownMinute : '+ countdownMinute);
        if ((countdownMinute != undefined)&&(countdownMinute < 50)){
          start();

          done = true;
          clearInterval(timer);
        }
      }, 5000);


    }
  }



})();

//labourchere.cron();
