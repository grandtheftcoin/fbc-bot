var Freebitcoin = (function(){
        
//	var balance = 0;
//	var countdownRoll = {
//		minute : null,
//		second : null
//	};
//	var timer = null;
//	
	//reward
	//var status
	
	
	return{
//		sync : function(){
//			timer = setInterval(function(){
//				balance = $('#balance').html();
//				countdownRoll.minute = $('#time_remaining .countdown_amount').eq(0).html();
//				countdownRoll.second = $('#time_remaining .countdown_amount').eq(1).html();
//			}, 1000);
//		},
//		pause : function(){
//			clearInterval(timer);
//		},
		getBalance : function(){
//                    var balancebonus = parseFloat($('#bonus_account_balance').html().replace(' BTC',''));
                    var balance = parseFloat($('#balance').html());
//                    return balance+balancebonus;
                    return balance;
		},
		getCountDownRoll : function(){
                    return $('#time_remaining .countdown_amount').eq(0).html();
		},
                getBetRollNumber : function(){
                    var digit1 = $('#multiplier_first_digit').html();
                    var digit2 = $('#multiplier_second_digit').html();
                    var digit3 = $('#multiplier_third_digit').html();
                    var digit4 = $('#multiplier_fourth_digit').html();
                    var digit5 = $('#multiplier_fifth_digit').html();
                    
                    var rollNumber = digit1+digit2+digit3+digit4+digit5;
                    return rollNumber;
                },
                getFreeRollNumber : function(){
                    var digit1 = $('#free_play_first_digit').html();
                    var digit2 = $('#free_play_second_digit').html();
                    var digit3 = $('#free_play_third_digit').html();
                    var digit4 = $('#free_play_fourth_digit').html();
                    var digit5 = $('#free_play_fifth_digit').html();
                    
                    var rollNumber = digit1+digit2+digit3+digit4+digit5;
                    return rollNumber;
                },
                getRollWinning : function(){
                    return $("#winnings").html();
                },
                getRollRPWinning : function(){
                    return $("#fp_reward_points_won").html();
                },
                getRewardPoint : function(){
                    return $( "div:contains('YOUR REWARD POINTS (RP)')" ).last().siblings().html().replace(/,/g, '');
                },
                getBonusIncreasePercent : function(){
                    return $("#fp_bonus_req_completed").html();
                },
                getMinReward : function(){
                    var minReward = $("#fp_min_reward").html();
//                    return minReward == undefined ? "0": minReward.match(/(0\.)\w+/g)[0];
                    return minReward == undefined ? "0": minReward.replace(' BTC','');
                },
                getBonusBalance : function(){
                    var bonusAccountBalance = $("#bonus_account_balance").html();
//                    return bonusAccountBalance == undefined ? "0" : bonusAccountBalance.match(/(0\.)\w+/g)[0];
                    return bonusAccountBalance == undefined ? "0" : bonusAccountBalance.replace(' BTC','');
                },
                getBonusWagerRemaining : function(){
                    var bonusAccountWager = $("#bonus_account_wager").html();
//                    return bonusAccountWager == undefined? "0" : bonusAccountWager.match(/(0\.)\w+/g)[0];
                    return bonusAccountWager == undefined? "0" : bonusAccountWager.replace(' BTC','');
                }
                
                
	}
})();

//Freebitcoin.sync();