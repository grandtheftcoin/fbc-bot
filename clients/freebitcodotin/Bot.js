var Bot = (function(){
  const intervalTimer = 5000;
  var timer = null;
  var PROD = 1;
  var DEV = 0;
  var env = DEV;
  var betCount = 0;
  
  var sleepHours = [1,2,3];
  var alarmTimer = null;
  var date = null;
  
  var start = function(){
        date = new Date();
        console.log("Bot Started");
        var currentHour = date.getHours();

        if(sleepHours.indexOf(currentHour) != -1){
            alarmTimer = setInterval(function(){
            
            date = new Date();
            currentHour = date.getHours();
            
            if(sleepHours.indexOf(currentHour) == -1){
                clearInterval(alarmTimer);
                location.reload(true);
            }
                
            }, intervalTimer);
            return;
        }
      
        if(!AutoRedeem.isHaveBonusRewardPoint() && (env == PROD)){
                AutoRedeem.redeemBonusRewardPoint();
                timer = setTimeout(start, intervalTimer);
        }
        else
        if(!AutoRedeem.isHaveBonusFreeCoin() && (env == PROD)){
              AutoRedeem.redeemBonusFreeCoin();
              timer = setTimeout(start, intervalTimer);
        }
        else
        if(AutoRoll.isRollAvailable() && AutoRedeem.isHaveBonusFreeCoin() && AutoRedeem.isHaveBonusRewardPoint() && (env == PROD)){
                AutoRoll.doRoll();
                timer = setTimeout(start, intervalTimer);
        }
        else{
              if(betCount == 0){
                betCount = 1;
                console.log("Labourchere start");
                Labourchere.start();  
              }

        }
  }
  
  return {
    start : function(){
//      AutoRedeem.setLog(true);
//      AutoRoll.setLog(true);
//      Helper.setLog(true);
        
        

      timer = setTimeout(start, intervalTimer);
    },
    stop : function(){
      clearTimeout(timer);
    },
    resetBet : function(){
        betCount = 0;
        this.start();
    }
  }
})();

window.onload = function() {
  Bot.start();
};