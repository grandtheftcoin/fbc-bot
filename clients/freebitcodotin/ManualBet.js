var manualBet = (function(){
  const intervalTimer = 1000;
  var showLog = false;



  function getRollNumber(){
    var first = document.getElementById('multiplier_first_digit').innerHTML;
    var second = document.getElementById('multiplier_second_digit').innerHTML;
    var third = document.getElementById('multiplier_third_digit').innerHTML;
    var fourth = document.getElementById('multiplier_fourth_digit').innerHTML;
    var fifth = document.getElementById('multiplier_fifth_digit').innerHTML;
    var rollNumber = first+second+third+fourth+fifth;

    return rollNumber;
  }

  function isRollingDone(){
    isRollingDone.newNumber = getRollNumber();
    var ret;
    ret = isRollingDone.newNumber == isRollingDone.oldNumber;
    isRollingDone.oldNumber = isRollingDone.newNumber;
    return ret;

  };





  return{
	  
  setTrend : function(rollNumber){
        trend.setLastRoll(rollNumber);
	 if(rollNumber < 4750){
		 trend.setInputResultLow();
	 }
	 else if (rollNumber > 5250){
		 trend.setInputResultHigh();
	 }
	 else{
		 trend.setInputResultNoState();
	 }
	  
  },
  betLow : function(){
      var clickDone = false;
      var _this = this;
      var promise = new Promise(function(resolve, reject){
        var timer = setInterval(function(){
          if(isRollingDone()&&clickDone){
            clearInterval(timer);
            var rollNumber = getRollNumber(); 
            var ret = (rollNumber < 4750);
            if(showLog)console.log('bet result : '+ret);
            
            trend.setInputLow();
            _this.setTrend(rollNumber);
            
            var retBet = {
                type : 'LOW',
                result : ret
            };
            
            return resolve(retBet);
          }
          else{

            if(clickDone){
              if(showLog)console.log('waiting roll');
            }
            else{
                if(showLog)console.log('betLow');
                $('#double_your_btc_bet_lo_button').click();
                clickDone = true;
            }

          }

        },intervalTimer);
      });
      return promise;
    },
    betHigh : function(){
    	var clickDone = false;
    	var _this = this;
        var promise = new Promise(function(resolve, reject){
          var timer = setInterval(function(){
            if(isRollingDone()&&clickDone){
              clearInterval(timer);
              var rollNumber = getRollNumber(); 
              
              
              var ret = ( rollNumber > 5250);
              if(showLog)console.log('bet result : '+ret);
              
              trend.setInputHigh();
              _this.setTrend(rollNumber);
              
              
            var retBet = {
              type : 'HIGH',
              result : ret
            };
              
              return resolve(retBet);
            }
            else{

              if(clickDone){
                if(showLog)console.log('waiting roll');
              }
              else{
                  if(showLog)console.log('betHigh');
                  $('#double_your_btc_bet_hi_button').click();
                  clickDone = true;
              }

            }

          },intervalTimer);
        });
        return promise;
    },
    
     setBetAmount : function(amount){
       var promise = new Promise(function(resolve, reject){
         var timer = setInterval(function(){
           if($('#double_your_btc_stake').val() == amount){
             clearInterval(timer);
             return resolve();
           }
           else{
             if(showLog)console.log('set bet amount : '+amount);
             $('#double_your_btc_stake').val(amount);
           }

         },intervalTimer);
       });
       return promise;
     },
    // openBetTab = openBetTab()
    //open multiply BTC tab
    openBetTab : function(){
      var promise = new Promise(function(resolve, reject){
        var timer = setInterval(function(){
          if($('#double_your_btc_tab').css('display') == "block"){
            clearInterval(timer);
            return resolve();
          }
          else{
            if(showLog)console.log('click tab bet');
            $('.top-bar-section .double_your_btc_link').click()
          }

        },intervalTimer);
      });
      return promise;
    }
  }
})();
