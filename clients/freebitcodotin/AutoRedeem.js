var AutoRedeem = (function(){
  var showLog = false;
  var busy = false;
  var intervalTimer = 1000;

  var openRewardTab = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#rewards_tab').css('display') == "block"){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('click tab reward');
          $('.rewards_link')[0].click()
        }

      },intervalTimer);
    });
    return promise;
  }

  var showRewardPointBonus = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#free_points_rewards').css('display') == "block"){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('show reward point bonus');
          $('#free_points_rewards').siblings('.reward_category_name').click();
        }

      },intervalTimer);
    });
    return promise;
  }


  var redeemRewardPointBonus = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#bonus_span_free_points').html() != undefined){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('redeem reward point bonus');
          RedeemRPProduct('free_points_100')
        }

      },intervalTimer);
    });
    return promise;
  }

  //free btc
  var showFreeBTCBonus = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#fp_bonus_rewards').css('display') == "block"){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('show free btc bonus');
          $('#fp_bonus_rewards').siblings('.reward_category_name').click();
        }

      },intervalTimer);
    });
    return promise;
  }


  var redeemFreeBTCBonus = function(){
    var promise = new Promise(function(resolve, reject){
      var timer = setInterval(function(){
        if($('#bonus_span_fp_bonus').html() != undefined){
          clearInterval(timer);
          return resolve();
        }
        else{
          if(showLog)console.log('redeem free btc bonus');
          RedeemRPProduct('fp_bonus_100');
        }

      },intervalTimer);
    });
    return promise;
  }

  return{
    isBusy : function(){
      return busy;
    },
    isHaveBonusRewardPoint : function(){
      return !($('#bonus_span_free_points').html() == undefined);
    },
    isHaveBonusFreeCoin : function(){
      return !($('#bonus_span_fp_bonus').html() == undefined);
    },
    redeemBonusRewardPoint : function(){
      if(busy)return;
      busy = true;
      //reward point bonus
      Helper.waitRandom()
      //1. open reward tab
      .then(openRewardTab)
      //2. show reward point bonus
      .then(showRewardPointBonus)
      //3. redeem
      .then(redeemRewardPointBonus)
      ;
      busy = false;
    },
    redeemBonusFreeCoin : function(){
      if(busy)return;
      busy = true;
      //reward point bonus
      Helper.waitRandom()
      //1. open reward tab
      .then(openRewardTab)
      //2. show free btc bonus
      .then(showFreeBTCBonus)
      //3. redeem
      .then(redeemFreeBTCBonus)
      .then(function(){
        clearInterval(redeemTimer);
      })
      ;
      busy = false;
    },
    setLog : function(status){
      showLog = status;
    }
  }

})();
