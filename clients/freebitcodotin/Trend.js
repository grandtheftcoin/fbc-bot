var trend = (function(){
	var HIGH = 1;
	var LOW = -1;
	var NO_STATE = 0;
	
	var rollResult = [];
	var roll = [];
        var lastRoll = 0;
	return{
                setLastRoll : function(number){
                    lastRoll = number;
                },
                getLastRoll : function (){
                    return lastRoll;
                },
		setInputResult : function(input){
			rollResult.push(input);
		},
		setInput : function(input){
			roll.push(input);
		},
		setInputResultHigh: function(){
			this.setInputResult(HIGH);
		},
		setInputHigh: function(){
			this.setInput(HIGH);
		},
		setInputResultLow: function(){
			this.setInputResult(LOW);
		},
		setInputLow: function(){
			this.setInput(LOW);
		},
		setInputResultNoState: function(){
			this.setInputResult(NO_STATE);
		},
		countLastTrend : function(num){
			var arrayLength = rollResult.length;
			if(arrayLength > num){
				var last = rollResult.slice((arrayLength-num), arrayLength);
				return this.countTrendChunk(last);
			}
			else{
				return this.countTrendChunk(rollResult);
			}
			
			
		},
		countTrend : function(){
			return this.countTrendChunk(rollResult);
		},
		countTrendChunk : function(rollResult){
			  var countHigh = 0;
			  var countLow = 0;
			  var countOutState = 0;

			  var countHighStreak = [];
			  var counterHighStreak = 0;
			  var statusHighStreak = false;

			  var countLowStreak = [];
			  var counterLowStreak = 0;
			  var statusLowStreak = false;

			  var alternateStreakBeginHigh = [];
			  var alternateStreakBeginLow = [];

			  var statusAlternateStreak = false;
			  var counterAlternateStreak = 0;
			  

			  rollResult.forEach(function(item, index){
//			    console.log('item : ',item);
//			    console.log('index : ', index);
//			    console.log('before : ', rollResult[index-1]);
//			    console.log('---------------------');

			    statusHighStreak = false;
			    statusLowStreak = false;
			    statusAlternateStreak = false;

			    if(item == 1){
			      countHigh++;
			      statusHighStreak = true;

			      if(rollResult[index-1] == -1){
			        statusAlternateStreak = true;
			      }
			    }
			    else if (item == -1){
			      countLow++;
			      statusLowStreak = true;

			      if(rollResult[index-1] == 1){
			        statusAlternateStreak = true;
			      }

			    }
			    else{
			      countOutState++;
			    }

			    //high streak count
			    if(statusHighStreak == true){
			      counterHighStreak++;
			    }
			    else if(statusHighStreak == false){
			      if(counterHighStreak > 1){
			        //save new record
			        countHighStreak.push(counterHighStreak);
//			        console.log('new high steak');

			      }
			      counterHighStreak = 0;

			    }

			    //low streak count
			    if(statusLowStreak == true){
			      counterLowStreak++;
			    }
			    else if(statusLowStreak == false){
			      if(counterLowStreak > 1){
			        //save new record
			        countLowStreak.push(counterLowStreak);
//			        console.log('new low steak');
			      }
			      counterLowStreak = 0;
			    }

			    //alternate streak count
			    if(statusAlternateStreak == true){
			      if(counterAlternateStreak == 0){
			        counterAlternateStreak = 2;
			      }
			      else{
			        counterAlternateStreak++;
			      }
			    }
			    else if (statusAlternateStreak == false){
			      if(counterAlternateStreak > 1){
//			        console.log('check alternate');
//			        console.log('counterAlternateStreak '+counterAlternateStreak);
//			        console.log('first '+rollResult[index-counterAlternateStreak]);
//			        console.log('index first '+(index-counterAlternateStreak));
			        //alternate begin high
			        if(rollResult[index-counterAlternateStreak] == 1){
			          alternateStreakBeginHigh.push(counterAlternateStreak);
			        }
			        //alternate begin low
			        else if(rollResult[index-counterAlternateStreak] == -1){
			          alternateStreakBeginLow.push(counterAlternateStreak);
			        }
			      }

			      counterAlternateStreak=0;
			    }


			    //condition last array
			    if(index == (rollResult.length-1)){
//			      console.log('last index');
			      if(counterLowStreak > 1){
			        //save new record
			        countLowStreak.push(counterLowStreak);
			        // console.log('new low steak');
			      }
			      if(counterHighStreak > 1){
			        //save new record
			        countHighStreak.push(counterHighStreak);
			        // console.log('new high steak');
			      }
			      if(counterAlternateStreak > 1){
			        //alternate begin high
			        if(rollResult[index-counterAlternateStreak] == 1){
			          alternateStreakBeginHigh.push(counterAlternateStreak);
			        }
			        //alternate begin low
			        else if(rollResult[index-counterAlternateStreak] == -1){
			          alternateStreakBeginLow.push(counterAlternateStreak);
			        }
			      }
			    }



			  });

			  //print
//			  console.log('roll :', roll);
//			  console.log('rollResult :', rollResult);
//			  console.log('countHigh : '+countHigh);
//			  console.log('countLow : '+countLow);
//			  console.log('countOutState : '+countOutState);

			  console.log('countHighStreak : '+countHighStreak);
			  console.log('countLowStreak : '+countLowStreak);
			  console.log('alternateStreakBeginHigh : '+alternateStreakBeginHigh);
			  console.log('alternateStreakBeginLow : '+alternateStreakBeginLow);
			  
//			  var MaxHighStreak =  Math.max(...countHighStreak);
//			  var MaxLowStreak =  Math.max(...countLowStreak);
//			  var MaxAlternateHighStreak =  Math.max(...alternateStreakBeginHigh);
//			  var MaxAlternateLowStreak =  Math.max(...alternateStreakBeginLow);
			  
			  var getSum = function(total, num){
				  return total+num;
			  }
			  
			  var sumHighStreak =  countHighStreak.length == 0 ?0:countHighStreak.reduce(getSum);
			  var sumLowStreak =  countLowStreak.length == 0 ? 0: countLowStreak.reduce(getSum);
			  var sumAlternateHighStreak =  alternateStreakBeginHigh.length == 0?0:alternateStreakBeginHigh.reduce(getSum);
			  var sumAlternateLowStreak =  alternateStreakBeginLow.length == 0?0:alternateStreakBeginLow.reduce(getSum);
			  
			  console.log('sumHighStreak : ',sumHighStreak);
			  console.log('sumLowStreak : ',sumLowStreak);
			  console.log('sumAlternateHighStreak : ',sumAlternateHighStreak);
			  console.log('sumAlternateLowStreak : ',sumAlternateLowStreak);
			  
			  var tempResult = [
				  sumHighStreak,
				  sumLowStreak,
				  sumAlternateHighStreak,
				  sumAlternateLowStreak
			  ];
			  
			  maxResultValue = Math.max(...tempResult);
			  
			  return tempResult.indexOf(maxResultValue);
			}
	}
})();
