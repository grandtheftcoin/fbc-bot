var AutoRoll = (function(){

  var showLog = false;
  // var isWaiting = false;
  var rollTimer = null;
  var busy = false;
  var intervalTimer = 1000;
  var isClick = false;

  var before = {
      rp : null,
      btc : null,
      percent : null
  };

  var waiting = function(){
    var countdownMinute = $('#time_remaining .countdown_amount').eq(0).html();
    if(showLog)console.log('countdownMinute : '+ countdownMinute);

    return (countdownMinute != undefined);
  }

  function getRollNumber(){
    var first = document.getElementById('multiplier_first_digit').innerHTML;
    var second = document.getElementById('multiplier_second_digit').innerHTML;
    var third = document.getElementById('multiplier_third_digit').innerHTML;
    var fourth = document.getElementById('multiplier_fourth_digit').innerHTML;
    var fifth = document.getElementById('multiplier_fifth_digit').innerHTML;
    var rollNumber = first+second+third+fourth+fifth;

    return rollNumber;
  }

  function doRoll(){
    var ret = new Promise(function(resolve, reject){
      var rollNumber = "";
        before.rp = Freebitcoin.getRewardPoint();
        before.btc = Freebitcoin.getBalance();
        before.percent = Freebitcoin.getBonusIncreasePercent();
        
        if(!isClick){
            isClick = true;
            $('#free_play_form_button').click();
            rollTimer = setInterval(function() {
                var newRollNumber = getRollNumber() ;
                if(newRollNumber === rollNumber){
                  clearInterval(rollTimer);
                  return resolve(newRollNumber);
                }
                else{
                  rollNumber = newRollNumber;
                }

            }, intervalTimer);
        }


    });
    return ret;
  }

    function saveLog(rollNumber){
        var promise = new Promise(function(resolve, reject){
                  
                  $.ajax({
                    method: "POST",
                    url: "http://127.0.0.1:3000/log_roll",
                    data: {
                        roll_number : rollNumber,
                        won_btc : Freebitcoin.getRollWinning(), 
                        won_rp : Freebitcoin.getRollRPWinning(), 
                        rp_before : before.rp, 
                        rp_after : Freebitcoin.getRewardPoint(), 
                        btc_before : before.btc, 
                        btc_after : Freebitcoin.getBalance(), 
                        bonus_inc_percent_before : before.percent, 
                        bonus_inc_percent_after : Freebitcoin.getBonusIncreasePercent()
                    }
                    })
                    .done(function( msg ) {
                      console.log("save log roll done");
                      resolve();
                    })
                    .error(function(request, status, error){
                        console.log("ajax error : "+error);
                    });
                  
          });
          return promise
    }

  return{
    isRollAvailable : function(){
      return !waiting();
    },
    doRoll : function(){

      Helper.waitRandom()
      .then(doRoll)
      .then(saveLog)
      
      ;
    },
    setLog : function(status){
      showLog = status;
    },
  }

})();

// autoRoll.start();
