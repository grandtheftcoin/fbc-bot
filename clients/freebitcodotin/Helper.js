var Helper = (function(){
  var showLog = false;
  return{
    waitRandom : function(){
        var promise = new Promise(function(resolve, reject){
        var waitTime = Math.floor((Math.random() * 10000) + 1000);
        if(showLog)console.log('wait time : '+waitTime);
        setTimeout(function(){
          return resolve();
        }, waitTime);

      });
      return promise;
    },
    setLog : function(status){
      showLog = status;
    }
  }
})();
