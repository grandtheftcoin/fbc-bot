/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = function (grunt) {
    // Project configuration.
    
    fileSrc = [
                    'clients/freebitcodotin/Helper.js', 
                    'clients/freebitcodotin/Freebitcoin.js' ,
                    'clients/freebitcodotin/AutoRedeem.js', 
                    'clients/freebitcodotin/AutoRoll.js', 
                    'clients/freebitcodotin/Stat.js', 
                    'clients/freebitcodotin/Trend.js', 
                    'clients/freebitcodotin/ManualBet.js', 
                    'clients/freebitcodotin/SequenceBet.js', 
                    'clients/freebitcodotin/Labouchere.js', 
                    'clients/freebitcodotin/Bot.js'
                ];
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat:{
            dist:{
                src : fileSrc,
                dest : 'clients/freebitcodotin/fbc.js'
            }
        },
        uglify:{
            dist:{
                src : fileSrc,
                dest : 'clients/freebitcodotin/fbc.min.js'
            }
        }
        
    });
    
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    
    grunt.registerTask('default', ['concat', 'uglify']);
};


